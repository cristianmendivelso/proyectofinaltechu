//Express - Libreria Web
var express = require('express'),
app = express(),
port = process.env.PORT || 3000;
const ProtectedRoutes = express.Router();



//Crypto - Libreria para tokenizacion y Morgan - Liberia de logs de peticiones REST
const crypto = require('crypto'),
config = require('./configuration/config'),
morgan = require('morgan'),
tokenList = [];
var hash = crypto.createHash('sha256');

//JWT - Libreria para generar token de sesiones, cors - Libreria de configuracion politicas de seguridad
var jwt = require('jsonwebtoken');
var bodyParser = require('body-parser');
var request = require('request-json');
var cors = require('cors');

//ShortId - Identificador unico y de pocos caracteres para cada objeto que se cree
const shortid = require('shortid');

var corsOptions = {
  "origin": "*",
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
  "preflightContinue": false,
  "optionsSuccessStatus": 204
}

app.listen(port);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '10mb' }));


// use morgan to log requests to the console
app.use(morgan('dev'));

app.set('Secret', config.secret);
app.set('Secret-Token', config.secretToken);

console.log('todo list RESTful API server started on: ' + port);



app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'))
});

app.post('/', function (req, res) {
  res.send("Hemos recibido su petición post")
});

app.put('/', function (req, res) {
  res.send("Hemos recibido su petición put cambiada");
});

app.delete('/', function (req, res) {
  res.send("Hemos recibido su petición delete")
});

app.get('/clientes/:idecliente', function (req, res) {
  res.send("Aqui tiene al cliente número " + req.params.idecliente);
});

//var movimientosJSON = require('./movimientosv1.json');
app.get('/v1/movimientos', function (req, res) {
  res.sendFile(path.join(__dirname, 'movimientosv1.json'));
  //tambien sirve res.send(movimientosJSON);
});

app.use(bodyParser.json());

app.use(cors());

app.post('/movimientos', function (req, res) {
  clienteMLab.post('', req.body, function (err, resM, body) {
    if (err) {
      console.log(err);
    } else {
      res.send(body);
    }
  });
});

var urlusuariosMLab = "https://api.mlab.com/api/1/databases/cmendivelso/collections/usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabUsuarios = request.createClient(urlusuariosMLab);
clienteMLabUsuarios.headers["Content-Type"] = "application/json";

function consultarExistenciaUsuarioEmail(email) {
  let usuarioEncontrado = false;
  let urlusuariosMLabConsult = urlusuariosMLab + "&q=" + JSON.stringify({ "email": email });
  let clienteMLabUsuariosConsult = request.createClient(urlusuariosMLabConsult);
  clienteMLabUsuariosConsult.headers["Content-Type"] = "application/json";
  let promesaConsultarUsuario = clienteMLabUsuariosConsult.get('');
  return promesaConsultarUsuario;
}

function refrescarToken(tokenAnterior, responseObject) {
  var nuevoToken;
  if (tokenAnterior['refresh']) {
    nuevoToken = jwt.sign({ 'email': tokenAnterior['email'] }, app.get('Secret-Token'), {
      expiresIn: 20000 // expires in 20 sec
    });
    responseObject["token"] = nuevoToken;
  }
}

app.use('/api', ProtectedRoutes);
//Dependencia para cargar recursos
var path = require('path');

ProtectedRoutes.use((req, res, next) => {
  // check header for the token
  var token = req.headers['authorization'];
  // decode token
  if (token && tokenList.indexOf(token) == -1) {
    // verifies secret and checks if the token is expired
    jwt.verify(token, app.get('Secret-Token'), (err, decoded) => {
      if (err) {
        console.log('Este es el JWT decoded');
        console.log(decoded);
        try {
          var decodedExpirationToken = jwt.verify(token, app.get('Secret-Token'), {
            ignoreExpiration: true //handled by OAuth2 server implementation
          });
          tokenList.push(token);
          decodedExpirationToken['refresh'] = true;
          req.decoded = decodedExpirationToken;
          next();
        } catch (e) {
          res.status(401).send({ message: 'Unauthorized' });
        }
      } else {
        console.log('Este es el JWT decoded');
        console.log(decoded);
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });
  } else {
    // if there is no token
    res.status(406).send({
      message: 'No Accept Request, Invalid Token'
    });
  }
});


var urlcuentasMLab = "https://api.mlab.com/api/1/databases/cmendivelso/collections/cuentas?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
function consultarCuentasxCliente(idCliente) {
  var urlcuentaMLab = urlcuentasMLab + "&q={'idCliente':'" + idCliente + "'}";
  var clienteMLabcuenta = request.createClient(urlcuentaMLab);
  let cuentas = clienteMLabcuenta.get('');
  return cuentas;

}

function consultarCuenta(idCliente, idCuenta) {
  var urlucuentaMLab = urlcuentasMLab + "&q={'idCliente':'" + idCliente + "','idCuenta':" + idCuenta + "}";
  console.log("urlucuentaMLab");
  console.log(urlucuentaMLab);

  var clienteMLabucuenta = request.createClient(urlucuentaMLab);
  let cuenta = clienteMLabucuenta.get('');
  return cuenta;
}

var urlclientesMLab = "https://api.mlab.com/api/1/databases/cmendivelso/collections/usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
function consultarCliente(correo) {
  var urlclienteMLab = urlclientesMLab + "&q={'email':'" + correo + "'}";
  var clienteMLabclientes = request.createClient(urlclienteMLab);
  let cliente = clienteMLabclientes.get('');
  return cliente;

}

//Consultar movimientos
ProtectedRoutes.get('/movimientos', function (req, res) {
  var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/cmendivelso/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
  var clienteMLab = request.createClient(urlmovimientosMLab);
  clienteMLab.headers["Content-Type"] = "application/json";
  clienteMLab.get('', function (err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      refrescarToken(req.decoded, body);
      res.send(body);
    }
  });
});

//Añadir un movimiento
ProtectedRoutes.post('/movimientos/:idcliente/:idcuenta', function (req, res) {
  console.log('putmovs');
  var cuenta = consultarCuenta(req.params.idcliente, req.params.idcuenta);
  cuenta.then(function (result) {
    result.body[0].listadoMovimientos.push(req.body);
    var urlputmovimientosMLab = "https://api.mlab.com/api/1/databases/cmendivelso/collections/cuentas?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt" + "&q={'idCliente':'" + req.params.idcliente+"','idCuenta':" + req.params.idcuenta + "}&u=true";
    console.log("urlputmovimientosMLab");
    console.log(urlputmovimientosMLab);
    var clientemovsMLab = request.createClient(urlputmovimientosMLab);
    console.log(req.body);
    clientemovsMLab.put('', result.body[0], function (err, res2, body) {
      if (err) {
        console.log(err);
      } else {
        refrescarToken(req.decoded, body);
        res.send(body);
      }
    });
  });
});

//Obtener movimientos
ProtectedRoutes.get('/movimientos/:idcliente/:idcuenta', function (req, res) {
  console.log('getmovs');
  var cuenta = consultarCuenta(req.params.idcliente, req.params.idcuenta);
  cuenta.then(function (result) {
    console.log("movimientos");
    result.body[0].listadoMovimientos;
    console.log(result.body[0].listadoMovimientos)
    var bodyTok = { listado:result.body[0].listadoMovimientos}
    refrescarToken(req.decoded, bodyTok);
    res.send(bodyTok)
  });

});



//Generar PDF
ProtectedRoutes.post('/movimientos/pdf', function (req, res) {
  console.log("pdff")
  var cli = consultarCliente(req['decoded']['email']);
  cli.then(function (result) {
    var urlPdfGenerator = "https://us1.pdfgeneratorapi.com/api/v3/templates/52395/output?output=url&name=extracto";
    var clientePdf = request.createClient(urlPdfGenerator);
    clientePdf.headers["Content-Type"] = "application/json; charset=utf-";
    clientePdf.headers["X-Auth-Workspace"] = "cristianfmendi@hotmail.com";
    clientePdf.headers["X-Auth-Secret"] = "96ce7c021cd97a38cbae35da652fb6a2c2b2b8060cd59c502d7cfb689f2c9e3a";
    clientePdf.headers["X-Auth-Key"] = "5bf8fef0a1e4970b1b1ea711973ab9680ca9308710ca015e07f74859fc0c5442";
    clientePdf.headers["Accept"] = "application/json";
    console.log("result.body");
    console.log(result.body);
    req.body['clienteNombre']=result.body[0].nombre +' '+ result.body[0].apellido;
    req.body['email']=req['decoded']['email'];
    console.log(req.body)
    clientePdf.post('', req.body, function (err, resM, body) {
      if (err) {
        console.log(err);
      } else {
        refrescarToken(req.decoded, body);
        res.send(body);
      }
    });
  })

});

//Crear Cuentas
ProtectedRoutes.post('/cuentas', function (req, res) {
  console.log(req['decoded']['email']);
  var consultaUsuario = consultarExistenciaUsuarioEmail(req['decoded']['email']);
  consultaUsuario.then(function (result) {
    var newbody = req.body;
    var idCliente = result.body[0].idCliente;
    newbody['idCliente'] = idCliente;

    var cuentas = consultarCuentasxCliente(idCliente);
    cuentas.then(function (result) {
      var idCuenta = result.body.length + 1;
      newbody['idCuenta'] = idCuenta;
      var clienteMLabcuentas = request.createClient(urlcuentasMLab);
      clienteMLabcuentas.post('', newbody, function (err, resM, body) {
        if (err) {
          console.log(body);
        } else {
          refrescarToken(req.decoded, body);
          res.send(body);
        }
      })
    });
  });
});

//Obtener Cuentas
ProtectedRoutes.get('/cuentas', function (req, res) {
  console.log(req['decoded']['email']);
  var consultaUsuario = consultarExistenciaUsuarioEmail(req['decoded']['email']);
  consultaUsuario.then(function (result) {
    var idCliente = result.body[0].idCliente;
    var cuentas = consultarCuentasxCliente(idCliente);
    cuentas.then(function (result) {
      var responseObjectGetCuentas = {"cuentas": result.body};
      console.log(responseObjectGetCuentas);
      refrescarToken(req.decoded, responseObjectGetCuentas);
      res.send(responseObjectGetCuentas);
    });
  });
});

app.post('/logout', cors(corsOptions), function (req, res) {
  var token = req.headers['authorization'];
  if (token) {
    if (tokenList.indexOf(token) == -1) {
      tokenList.push(token);
    }
    res.status(205).send();
  } else {
    //Forbidden token
    res.status(403).send();
  }
});

app.post('/login', cors(corsOptions), function (req, res) {
  var consultaUsuario = consultarExistenciaUsuarioEmail(req.body.email);
  consultaUsuario.then(function (result) {
    if (result.body.length > 0) {
      var usuarioEncontrado = result.body[0];
      var passwordIngresado = crypto.createHmac('sha512', app.get('Secret'))
      .update(req.body.password)
      .digest('hex');
      if (usuarioEncontrado.password == passwordIngresado) {
        var tokenData = {
          email: usuarioEncontrado.email
        }
        //expiresIn: 60 * 60 * 24 // expires in 24 hours
        var token = jwt.sign(tokenData, app.get('Secret-Token'), {
          expiresIn: 20 // expires in 20 seg
        });

        var response = { 'message': 'Autenticacion realizada', 'token': token };
        res.status(200).send(response);
      } else {
        res.status(401).send({ 'message': 'Usuario o clave invalida' });
      }
    } else {
      res.status(400).send({ 'message': 'Error al procesar la solicitud' });
    }
  });
});


app.post('/usuarios', cors(corsOptions), function (req, res) {
  var consultaUsuario = consultarExistenciaUsuarioEmail(req.body.email);
  consultaUsuario.then(function (result) {
    if (result.body.length > 0) {
      res.status(409).send({ 'message': 'Usuario ya existente' });
    } else {
      var passwordHash = crypto.createHmac('sha512', app.get('Secret'))
      .update(req.body.password)
      .digest('hex');
      req.body.password = passwordHash;
      var usuarioId = shortid.generate();
      var usuarioDTO = { 'idCliente': usuarioId, 'nombre': req.body.nombre, 'apellido': req.body.apellido, 'email': req.body.email, 'password': req.body.password };
      clienteMLabUsuarios.post('', usuarioDTO, function (err, resM, body) {
        if (err) {
          res.status(400).send({ 'message': 'Error al procesar la solicitud' });
        } else {
          res.status(201).send({ 'id': usuarioId });
        }
      });
    }
  });
});
